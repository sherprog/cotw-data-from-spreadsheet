function drawMap(bus) {
  if (bus) {
    var queryString = '/gviz/tq?tq='+encodeURIComponent('SELECT A, B, C, D WHERE E = ' + bus);
    console.log(queryString);
    var query = new google.visualization.Query('https://docs.google.com/spreadsheets/d/18B_KHloQZiW2rrWanEZNdD1ScLp4XwDC43IwbfY9--Y/' + queryString);
    query.send(handleDataQueryResponse);
  } else {
    var queryString = '/gviz/tq?tq='+encodeURIComponent('SELECT A, B, C, D');
    console.log(queryString);
    var query = new google.visualization.Query('https://docs.google.com/spreadsheets/d/18B_KHloQZiW2rrWanEZNdD1ScLp4XwDC43IwbfY9--Y/' + queryString);
    query.send(handleDataQueryResponse);
  }
}

function handleDataQueryResponse(response) {
  console.log("Am I being called?");
  if (response.isError()) {
    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
    return;
  }

  var data = response.getDataTable();
  var map = new google.visualization.Map(document.getElementById('spreadsheetdata'));

var url = 'http://maps.google.com/mapfiles/ms/micons/';

  var options = {
    showTooltip: true,
    showInfoWindow: true,
    icons: {
      blue: {
        normal:   url + 'blue.png',
        selected: url + 'blue-dot.png'
      },
      green: {
        normal:   url + 'green.png',
        selected: url + 'green-dot.png'
      },
      pink: {
        normal:   url + 'pink.png',
        selected: url + 'pink-dot.png'
      },
      yellow: {
        normal:   url + 'yellow.png',
        selected: url + 'yellow-dot.png'
      },
      orange: {
        normal:   url + 'orange.png',
        selected: url + 'orange-dot.png'
      },
      red: {
        normal:   url + 'red.png',
        selected: url + 'red-dot.png'
      },
      purple: {
        normal:   url + 'purple.png',
        selected: url + 'purple-dot.png'
      },
      lightblue: {
        normal:   url + 'lightblue.png',
        selected: url + 'ltblue-dot.png'
      },
      brown: {
        normal:   'brown.png',
        selected: 'brown-dot.png'
      },
      gray: {
        normal:   'gray.png',
        selected: 'gray-dot.png'
      },
      crimson: {
        normal:   'crimson.png',
        selected: 'crimson-dot.png'
      }
    }
  };

  // Fixed: The id provide to getElementById was still the id from the demo.
  // I replaced it with the id from your html.
  map.draw(data, options);
}
console.log("Is this working?");
